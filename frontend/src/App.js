import React, { useRef, useState } from 'react';

const App = () => {
  const [todos, setTodos] = useState([]);
  const [newTodo, setNewTodo] = useState('');

  const [searchTodo, setSearchTodo] = useState('');
  const [searchTodoStatus, setSearchTodoStatus] = useState(false);

  const [todoCompleted, setTodoCompleted] = useState(false);

  const todoRef = useRef();

  const searchTodoRef = useRef();

  const todoTextChanged = (e) => {
    setNewTodo(e.target.value);
  }

  const handleAddTodo = () => {
    if (!newTodo) return;
    // setTodos([...todos, {  id: new Date().getTime(), title: newTodo, completed: false }]);
    setTodos(todos => [...todos, { id: new Date().getTime(), title: newTodo, completed: false, isSearch: false }]);
    setNewTodo('');

    todoRef.current?.focus();
  }

  const handleDeleteTodo = (id) => {
    setTodos((todos) => todos.filter((t) => t.id !== id))
  }

  const handleClearTodo = () => {
    setTodos([]);
  }

  const searchTodoTextChanged = (e) => {
    setSearchTodo(e.target.value);
  }

  const handleFilterTodo = (action) => {
    if (action === "isCompleted") {
      setTodoCompleted(!todoCompleted);
    }
    else if (action === "Search") {
      setSearchTodoStatus(!searchTodoStatus);
    }
  }

  console.log(todoCompleted);

  let filteredTodos = searchTodoStatus ? todos.filter((myTodo) => myTodo.title.includes(searchTodo)) : todos;
  filteredTodos = todoCompleted ? filteredTodos.filter((myTodo) => myTodo.completed === false) : filteredTodos;

  // const handleSearchTodo = () => {
  // setTodos((todos) => todos.filter((myTodo) => myTodo.title.includes(newTodo)));
  // setTodos((todos) =>
  //   todos.map((myTodos) => {
  //     if (myTodos.title.includes(newTodo)) return {...myTodos, }
  //   }
  //   )
  // );

  // setTodos((todos) =>
  //   todos.map((myTodos) =>
  //     myTodos.title.includes(newTodo) ? { ...myTodos, isSearch: false } : myTodos
  //   )
  // );

  // setIsSearching(true);
  // }

  //   const handleToggleShowCompleted = () => {
  //     setHideSelected(!hideSelected);
  //   }

  //   // const filteredTodos = hideSelected ? todos : todos.filter(todo => !todo.completed);

  const handleToggleTodo = (id, completed) => {
    completed = !completed;
    setTodos((todos) =>
      todos.map((myTodos) =>
        myTodos.id === id ? { ...myTodos, completed } : myTodos
      )
    );
  }


  console.log(todos);

  return (
    <div>
      <h1>Todo List</h1>

      <div>
        <input
          type="text"
          value={newTodo}
          onChange={todoTextChanged}
          ref={todoRef}
        />
        <button onClick={handleAddTodo}>Add Todo</button>
        <button onClick={handleClearTodo}>Clear Todo</button>
        <button onClick={() => handleFilterTodo('isCompleted')}> {todoCompleted ? 'Show All' : 'Show only task not done'}</button>
        <br />
        <input
          type="text"
          value={searchTodo}
          onChange={searchTodoTextChanged}
          ref={searchTodoRef}
        />
        <button onClick={() => handleFilterTodo('Search')}> {searchTodoStatus ? 'Clear Filter' : 'Filter Todo'}</button>
      </div>

      {filteredTodos.map((todo) => (
        <div key={todo.id}>
          <input
            type="checkbox"
            checked={todo.completed}
            onChange={() => handleToggleTodo(todo.id, todo.completed)}
          />

          <label style={{ textDecoration: todo.completed ? 'line-through' : 'none' }}>{todo.title}</label>

          <button onClick={() => handleDeleteTodo(todo.id)}> X </button>

        </div>
      ))}


    </div>
  );
}

export default App;