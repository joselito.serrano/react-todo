import express from "express";
import { z } from "zod";
import { todoCollection } from "../db.js";
import { ObjectId } from "mongodb";

const router = express.Router();

const TodoSchema = z.object({
  task: z.string(),
  checked: z.boolean(),
});

// url = /todos | method = GET
// Get all todo
router.get("/todos", async (req, res) => {
  const todo = await todoCollection.find().toArray();
  res.status(200).send(todo);
});

// url = /todos/{id} | method = GET
// Get todo based on id
router.get("/todos/:id", async (req, res) => {
  const todoId = req.params.id;

  // Check if todoId is a valid id based on the ObjectId that was imported from mongodb
  if (!ObjectId.isValid(todoId)) return res.status(400).send("Invalid ID");

  const foundTodo = await todoCollection.findOne({ _id: new ObjectId(todoId) });
  if (foundTodo == null) return res.status(404).send("Not Found");

  res.status(200).send(foundTodo);
});

// url = /todos | method = POST
// Create a todo
router.post("/todos", async (req, res) => {
  const newTodo = { ...req.body, checked: false };

  const parsedResult = TodoSchema.safeParse(newTodo);

  if (!parsedResult.success) return res.status(400).send(parsedResult.error);

  const result = await todoCollection.insertOne(parsedResult.data);
  const { insertedId } = result;
  const todoItem = await todoCollection.findOne({
    _id: new ObjectId(insertedId),
  });

  res.status(201).send(todoItem);
});

// url = /todos/{id} method = PATCH(POST)
// Update the checked status of a todo
router.patch("/todos/:id", async (req, res) => {
  const todoId = req.params.id;
  // { xxx } should be the same name as the Schema
  const { checked } = req.body;

  // Check if todoId is a valid id based on the ObjectId that was imported from mongodb
  if (!ObjectId.isValid(todoId)) return res.status(400).send("Invalid ID");

  const foundTodo = await todoCollection.findOne({ _id: new ObjectId(todoId) });
  if (foundTodo == null) return res.status(404).send("Not Found");

  const parsedResult = TodoSchema.safeParse({ ...foundTodo, checked });
  console.log(parsedResult);

  if (!parsedResult.success) return res.status(400).send(parsedResult.error);

  await todoCollection.updateOne(
    { _id: new ObjectId(todoId) },
    { $set: { checked } }
  );
  const todoItem = await todoCollection.findOne({ _id: new ObjectId(todoId) });
  res.status(200).send(todoItem);
});

// url = /todos/{id} method = DELETE(POST)
// Delete a todo using id
router.delete("/todos/:id", async (req, res) => {
  const todoId = req.params.id;

  const foundTodo = await todoCollection.findOne({ _id: new ObjectId(todoId) });
  if (foundTodo == null) return res.status(404).send("Not Found");

  await todoCollection.deleteOne({ _id: new ObjectId(todoId) });
  res.status(204).send();
});

// url = /todos method = DELETE(POST)
// Delete all todo
router.delete("/todos", async (req, res) => {
  await todoCollection.deleteMany({});
  res.status(204).send();
});

export default router;
